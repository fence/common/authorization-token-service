<?php

declare(strict_types=1);

namespace Glance\AuthorizationTokenService\AuthorizationToken;

use DomainException;
use GuzzleHttp\Client;

class AuthorizationTokenProvider
{
    private const DOMAIN = "https://auth.cern.ch/auth/realms/cern/api-access/token";

    public static function getAuthorizationToken(
        Client $client,
        string $clientId,
        string $clientSecret,
        string $audience
    ): string {
        $options = [
            "form_params" => [
                "grant_type" => "client_credentials",
                "client_id" => $clientId,
                "client_secret" => $clientSecret,
                "audience" => $audience,
            ],
        ];

        try {
            $response = $client->post(
                self::DOMAIN,
                $options
            );
            return json_decode($response->getBody()->getContents(), true)["access_token"];
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }
    }
}
